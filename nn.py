from torch._C import dtype
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch as T

class LinearClassifier(nn.Module):

    def __init__(self, lr, n_classes, input_dims):
        self = super().__init__()
        self.fc1 = nn.Linear(*input_dims, 128)
        self.fc2 = nn.Linear(128, 256)
        self.fc3 = nn.Linear(256, n_classes)

        self.optimizer = optim.adam(self.parameters(), lr=lr)
        self.loss = nn.CrossEntropyLoss()
        self.device = T.device('cuda:0' if T.cuda.is_available() else 'cpu')
        self.to(self.device)

    def forward(self, data):
        layer1 = F.sigmoid(self.fc1(data))
        layer2 = F.sigmoid(self.fc2(layer1))
        layer3 = self.fc3(layer2)

        return layer3

    def learn(self, data, labels):
        self.optimizer.zero_grad() # resetting optimizer gradients
        data = T.tensor(data).to(self.device) # casting
        labels = T.tensor(labels).to(self.device) # casting

        predictions = self.forward(data) # predict
        cost = self.loss(predictions, labels) # calculate loss
        
        cost.backward() # backward propagation
        self.optimizer.step() # update gradient

class FrozenLakeNeuralNetwork(nn.Module):

    HIDDEN_LAYER_NODES = 128 

    def __init__(self, num_states: int, num_actions: int, learning_rate: float = 0.001):
        super().__init__()
        self.input_layer = nn.Linear(num_states, self.HIDDEN_LAYER_NODES)
        self.output_layer = nn.Linear(self.HIDDEN_LAYER_NODES, num_actions)
        self.optimizer = optim.Adam(self.parameters(), lr=learning_rate)
        self.loss = nn.MSELoss()
        self.device = T.device('cuda:0' if T.cuda.is_available() else 'cpu')
        self.to(self.device)

    def forward(self, data):
        layer1 =  F.relu(self.input_layer(data))
        return self.output_layer(layer1) 

    def learn(self, data, labels):
        self.optimizer.zero_grad()
        data = T.tensor(data, dtype=T.float).to(self.device) # casting
        labels = T.tensor(labels).to(self.device) # casting

        predicitons = self.forward(data)
        cost = self.loss(predicitons, labels)
        cost.backward()
        self.optimizer.step()


