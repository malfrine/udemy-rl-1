
from q_learning_agent import FrozenLakeBaseAgent, FrozenLakeDeeplQLearningAgent, FrozenLakeQLearningAgent
import gym
import numpy as np
import matplotlib.pyplot as plt
import json


class RandomAgent(FrozenLakeBaseAgent):
    # default behaviour is random    
    pass


class DeterministicAgent(FrozenLakeBaseAgent):
    
    def get_index(self, observation):
        return observation // 4, observation % 4


    def get_action(self, env, observation):
        i, j = self.get_index(observation)
        print(f"Current index {i ,j}")
        # always try go down and to the right
        if i <= j: # vertically farther to goal than horizontally
            return 1
        else:
            return 2


def run_frozen_lake(agent: FrozenLakeBaseAgent):
    env = gym.make('FrozenLake-v0')
    observation = env.reset()
    t = -1
    done = False
    while not done:
        t += 1
        action = agent.get_action(env, observation)
        # print(f"At {observation}, agent chose {action}")
        new_observation, reward, done, _ =  env.step(action)
        # print(f"Agent state: {observation}, Agent new state: {new_observation}, Action: {action}, Reward: {reward}")
        agent.learn(observation, new_observation, action, reward)
        observation = new_observation
    env.close()
    return reward


def save_moving_average_win_rate_plot(scores, num_trailing=10):
    moving_average = np.array([
        np.mean(scores[max(0, t - num_trailing):(t + 1)])
        for t in range(len(scores))
    ])
    plt.plot(moving_average)
    plt.ylabel(f"Moving Average Win Rate")
    plt.xlabel(f"Number Games")
    plt.savefig("win-rate.png")


def run_all_frozen_lake_episodes(agent, num_games=1000, log_every=1):
    scores = list()
    for game in range(num_games):
        reward = run_frozen_lake(agent)
        scores.append(reward)
        if game % log_every == 0:
            print(f"Ran game {game}; Most recent reward is {reward}")
            print(f"{agent.table}")
            with open("agent.json", "w", encoding="utf-8") as f:
                f.write(agent.json(ensure_ascii=False, indent=4))
    return scores

def lecture4_task1():

    scores = run_all_frozen_lake_episodes(RandomAgent())
    print(f"Average Win Rate: {np.mean(scores)}")
    save_moving_average_win_rate_plot(scores)

def lecture5_task1():

    # LEFT = 0, DOWN = 1, RIGHT = 2, UP = 3
    agent = DeterministicAgent()
    
    assert agent.get_index(0) == (0, 0)
    assert agent.get_index(15) == (3, 3)
    assert agent.get_index(6) == (1, 2)

    assert agent.get_action(None, 0) == 1
    assert agent.get_action(None, 1) == 1
    assert agent.get_action(None, 4) == 2

    scores = run_all_frozen_lake_episodes(agent)
    print(f"Average Win Rate: {np.mean(scores)}")
    save_moving_average_win_rate_plot(scores)


def lecture9_task1():
    agent = FrozenLakeQLearningAgent()
    scores = run_all_frozen_lake_episodes(agent, num_games=5_000_000, log_every=5000)
    with open("agent.json", "w", encoding="utf-8") as f:
        f.write(agent.json(ensure_ascii=False, indent=4))
    print(f"Average Win Rate: {np.mean(scores)}")
    save_moving_average_win_rate_plot(scores)

def deep_learning_frozen_lake():
    agent = FrozenLakeDeeplQLearningAgent()
    scores = run_all_frozen_lake_episodes(agent, num_games=1, log_every=1)
    print(scores)

if __name__ == "__main__":
    deep_learning_frozen_lake()