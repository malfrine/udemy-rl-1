from nn import FrozenLakeNeuralNetwork
from typing import Dict, NewType
from pydantic import BaseModel
from random import random, choice

from pydantic.fields import Field
import torch as T

MAX_GREEDINESS = 1.0
MIN_GREEDINESS = 0.01
GREEDINESS_DECREASE_RATE = 0.000001
DEFAULT_STEP_SIZE = 0.001
DEFAULT_DISCOUNT_FACTOR = 0.9

State = NewType("State", int)
Action = NewType("Action", int)


class FrozenLakeBaseAgent:

    def get_all_possible_actions(self):
        return range(4)

    def learn(self, this_state: State, next_state: State, action: Action, reward: float):
        # does not learn
        pass

    def get_action(self, env, state: State):
        return self.env.action_space.sample()



class FrozenLakeQLearningAgent(BaseModel, FrozenLakeBaseAgent):
    table: Dict[State, Dict[Action, float]] = Field(default_factory=dict) # table where action-value function is learned
    step_size: float = DEFAULT_STEP_SIZE
    discount_factor: float = DEFAULT_DISCOUNT_FACTOR
    greediness: float = MAX_GREEDINESS
    greediness_decrease_rate: float = GREEDINESS_DECREASE_RATE
    

    def get_action(self, env, state: State):
        if random() < self.greediness:
            # choose random action
            action = choice(self.get_all_possible_actions())
        else:
            # choose best action
            action = self.get_best_action(state)
        return action

    def get_action_values(self, state: State):
        if state not in self.table:
            self.table[state] = {
                a: 0 for a in self.get_all_possible_actions()
            }
        return self.table[state]
        

    def get_best_action(self, state: State):
        action_values = self.get_action_values(state)
        best_action, _ = max(action_values.items(), key=lambda x: x[1])
        return best_action
    
    def get_state_action_value(self, state: State, action: Action):
        action_values = self.get_action_values(state)
        return action_values[action]

    def set_action_state_value(self, state: State, action: Action, reward):
        action_values = self.get_action_values(state)
        action_values[action] = reward
        
    def decrease_greediness(self):
        self.greediness = max(MIN_GREEDINESS, self.greediness - self.greediness_decrease_rate)

    def learn(self, this_state: State, next_state: State, action: Action, reward: float):
        cur_state_action_value = self.get_state_action_value(this_state, action)
        
        best_action_at_next_state = self.get_best_action(next_state)
        best_value_at_next_state = self.get_state_action_value(next_state, best_action_at_next_state)
        
        change = self.step_size * (reward + self.discount_factor * best_value_at_next_state - cur_state_action_value)


        updated_state_action_value = cur_state_action_value + change

        self.set_action_state_value(this_state, action, updated_state_action_value)
        self.decrease_greediness()

    

class FrozenLakeDeeplQLearningAgent(FrozenLakeQLearningAgent):

    nn: FrozenLakeNeuralNetwork = None

    def __init__(self, **data) -> None:
        super().__init__(**data)
        if self.nn is None:
            self.nn = FrozenLakeNeuralNetwork(num_actions=4, num_states=16)
    
    def get_best_action(self, state: State):
        state = T.tensor(state, dtype=T.float).to(self.nn.device)
        actions = self.nn.forward(state)
        action = T.argmax(actions).item()
        return action

    def get_state_action_value(self, state: State, action: Action):
        self.nn.forward()

    def learn(self, this_state: State, next_state: State, action: Action, reward: float):
        self.nn.optimizer.zero_grad()
        this_state = T.tensor(this_state).to(self.nn.device)
        next_state = T.tensor(next_state).to(self.nn.device)
        action = T.tensor(action).to(self.nn.device)
        reward = T.tensor(reward).to(self.nn.device)
        self.nn.learn(this_state, reward)
        return super().learn(this_state, next_state, action, reward)

    class Config:
        arbitrary_types_allowed = True

    
        


